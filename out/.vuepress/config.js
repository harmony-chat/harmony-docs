module.exports = {
  title: "Harmony",
  description:
    "The high-performance messaging app built for the modern real-time web!",
  themeConfig: {
    nav: [{text: "Home", link: "/"}],
    sidebar: require("../config.json")
  },
  plugins: ["@vuepress/register-components"]
};

console.log("Awoo!", module.exports);
