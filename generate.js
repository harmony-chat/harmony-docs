const fs = require("fs-extra");
const path = require("path");

async function enumerate(directory) {
  const results = new Set();
  const promises = new Set();

  const files = await fs.readdir(directory);
  for (const filename of files) {
    if (filename.endsWith(".js")) {
      // It's documentation, add it in
      results.add(directory + "/" + filename);
    } else if (!filename.includes(".") && filename != "_types") {
      promises.add(
        (async () => {
          const nextResults = await enumerate(directory + "/" + filename);
          for (const result of nextResults.values()) {
            results.add(result);
          }
        })()
      );
    }
  }
  await Promise.all(promises);
  return results;
}

async function run(documentationDirectory, outputDir) {
  outputDir = path.normalize(outputDir) + "/";
  documentationDirectory = path.normalize(
    documentationDirectory.startsWith(".")
      ? process.cwd() + "/" + documentationDirectory
      : documentationDirectory
  );
  const allCompletePaths = await enumerate(documentationDirectory);
  try {
    require(documentationDirectory + "/_types");
  } catch (err) {
    console.error("Error while inserting new types!", err);
    throw err;
  }

  const documentation = new Map();

  for (const filepath of allCompletePaths.values()) {
    const routePath = path.relative(documentationDirectory, filepath);
    const parsed = path.parse(routePath);
    const routeLocation = "/" + parsed.dir; // /channels/:channelId/messages
    const routeMethod = parsed.name; // GET, POST, PUT, etc.

    try {
      var routeData = require(filepath);
    } catch (err) {
      console.error(`Error while parsing: ${routePath}!`, err);
      throw err;
    }

    console.log("BUILDING DOCS FOR...", routePath);

    //import Route from "$/${routePath}";
    //import Playground from "@/Playground";

    const output = `# ${routeMethod} \`${routeLocation}\`:
---

${routeData.description}
${
      routeData.request
        ? `
## Request
\`\`\`json
${formatSchema(routeData.request, true)}
\`\`\``
        : ""
    }

## Response
\`\`\`json
${formatSchema(routeData.response, true)}
\`\`\`
`;

    // <Playground
    //   location="${routeLocation}"
    //   method="${routeMethod}"
    //   request={Route.request}
    //   response={Route.response} />

    documentation.set(
      {
        routeLocation,
        routeMethod
      },
      output
    );
  }

  console.log("Writing back documentation to disk...");
  const topLevel = new Map();
  for (const [metadata, document] of documentation.entries()) {
    await fs.mkdirp(outputDir + metadata.routeLocation);
    await fs.writeFile(
      outputDir + metadata.routeLocation + "/" + metadata.routeMethod + ".md",
      document,
      "utf8"
    );
    const topLevelName = metadata.routeLocation.substring(
      1,
      metadata.routeLocation.startsWith("/")
        ? metadata.routeLocation.substring(1).indexOf("/") + 1
        : metadata.routeLocation.indexOf("/")
    );
    const existing = topLevel.get(topLevelName);
    if (!existing) topLevel.set(topLevelName, new Set([metadata]));
    else existing.add(metadata);
  }

  const sidebarGroups = [];
  for (const [sidebarName, sidebarContents] of topLevel.entries()) {
    sidebarGroups.push({
      title: sidebarName,
      children: Array.from(sidebarContents.values()).map(meta => [
        meta.routeLocation + "/" + meta.routeMethod,
        meta.routeMethod + " " + meta.routeLocation
      ])
    });
  }
  await fs.writeFile(
    outputDir + "config.json",
    JSON.stringify(sidebarGroups, null, 2)
  );

  return documentation;
}

async function enumerateGroups(currentPath) {
  const output = [];

  const files = await fs.readdir(currentPath);
  for (const file of files) {
    if (file == "_types") continue;
    const niceFilename = path.parse(file).name;
    const finalPath = currentPath + "/" + niceFilename;
    if (file.includes(".")) {
      output.push([finalPath, niceFilename]);
    } else {
      output.push({
        title: niceFilename,
        children: await enumerateGroups(finalPath)
      });
    }
  }
  return output;
}

function formatSchema(schema, topLevel = false, indentation = "") {
  let output = "";
  const nextIndentation = indentation + "  ";
  if (schema.fields) {
    output += `{\n`;
    for (const fieldName in schema.fields) {
      output += `${nextIndentation}"${fieldName}": ${formatSchema(
        schema.fields[fieldName],
        false,
        nextIndentation
      )}`;
    }
    output += `${indentation}}`;
  } else {
    const lastSchemaIndex = schema.constructor.name.lastIndexOf("Schema");
    output += schema.constructor.name.substring(
      0,
      lastSchemaIndex === -1 ? undefined : lastSchemaIndex
    );
    if (schema._subType) {
      output += "<" + formatSchema(schema._subType, true, indentation) + ">";
    }

    if (schema.tests && schema.tests.length) {
      const formattedTests = schema.tests
        .filter(test => test.TEST && test.TEST.name)
        .map(test => {
          if (test.TEST.params) {
            return test.TEST.name + "=" + test.TEST.params[test.TEST.name];
          } else return test.TEST.name;
        })
        .filter((test, index, self) => self.lastIndexOf(test) == index);
      if (formattedTests.length) {
        output += "[" + formattedTests.join(", ") + "]";
      }
    }
  }
  if (!topLevel) output += ",\n";
  return output;
}

module.exports = {
  formatSchema,
  run,
  enumerate
};

process.on("unhandledRejection", err => console.log(err));
